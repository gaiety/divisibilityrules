def divide_by_one(num): # Is whole number
    return float(num).is_integer()

def divide_by_two(num): # Take last digit, divide by two only on that
    return _get_last_digit(num) % 2 == 0

def divide_by_three(num): # Sum each digit in number, then checking that sum is divisible by three
    return _sum_digits(num) % 3 == 0

def divide_by_four(num): # If last two digits are divisible by four, plus a simplifier of multiplying the tens place by two
    if (num <= 10): return num % 4 == 0
    tens_place = _get_last_digit(num, 2) * 10
    ones_place = _get_last_digit(num)
    num_to_check = (tens_place * 2) + ones_place
    return num_to_check % 4 == 0

def divide_by_five(num): # If last digit is a zero or a five return true
    last_digit = _get_last_digit(num)
    return last_digit == 0 or last_digit == 5

def divide_by_six(num): # Same rules as dividing by three, and ensure original number is even
    num_sum = _sum_digits(num)
    return num_sum % 3 == 0 and divide_by_two(num)

def divide_by_seven(num): # Multiply last digit by five then add unused digits to result, dividing the result by seven.
    if (num <= 100): return num % 7 == 0
    num_as_string = f"{num}"
    ones_place = _get_last_digit(num) * 5
    num_to_check = int(f"{num_as_string[:-1]}{ones_place}")
    return num_to_check % 7 == 0

def divide_by_eight(num): # Same rules as dividing by four, but for last three digits
    if (num <= 100): return num % 8 == 0
    hundreds_place = _get_last_digit(num, 3) * 100
    tens_place = _get_last_digit(num, 2) * 10
    ones_place = _get_last_digit(num)
    num_to_check = (hundreds_place * 4) + (tens_place * 2) + ones_place
    return num_to_check % 8 == 0

def divide_by_nine(num): # Same rules as dividing by three, but checking that sum is divisible by nine
    return _sum_digits(num) % 9 == 0

def _get_last_digit(num, indexFromEnd = 1):
    return int(f"{num}"[-1 * indexFromEnd])

def _sum_digits(num):
    num_string = f"{num}"
    num_sum = 0
    for digit_char in num_string:
        num_sum += int(digit_char)
    return num_sum

