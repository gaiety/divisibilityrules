# Divisibility Rules

Inspired by [VSauce's video on Divisibility Rules](https://www.youtube.com/watch?v=f6tHqOmIj1E)

[![Preview](http://i3.ytimg.com/vi/f6tHqOmIj1E/maxresdefault.jpg)](https://www.youtube.com/watch?v=f6tHqOmIj1E)

---

Requires Python 3 or later.

## Running Tests

```bash
python -m unittest tests/*_test.py
```
