from main import divide_by_seven
import unittest

class TestDivideBySeven(unittest.TestCase):
    def test_by_1(self):
        self.assertFalse(divide_by_seven(1))
    def test_by_2(self):
        self.assertFalse(divide_by_seven(2))
    def test_by_3(self):
        self.assertFalse(divide_by_seven(3))
    def test_by_7(self):
        self.assertTrue(divide_by_seven(7))
    def test_by_14(self):
        self.assertTrue(divide_by_seven(14))
    def test_by_700(self):
        self.assertTrue(divide_by_seven(700))
    def test_by_701(self):
        self.assertFalse(divide_by_seven(701))
    def test_by_7000(self):
        self.assertTrue(divide_by_seven(7000))
    def test_by_362880(self):
        self.assertTrue(divide_by_seven(362880))
