from main import divide_by_eight
import unittest

class TestDivideByEight(unittest.TestCase):
    def test_by_1(self):
        self.assertFalse(divide_by_eight(1))
    def test_by_2(self):
        self.assertFalse(divide_by_eight(2))
    def test_by_4(self):
        self.assertFalse(divide_by_eight(4))
    def test_by_8(self):
        self.assertTrue(divide_by_eight(8))
    def test_by_10(self):
        self.assertFalse(divide_by_eight(10))
    def test_by_16(self):
        self.assertTrue(divide_by_eight(16))
    def test_by_100(self):
        self.assertFalse(divide_by_eight(100))
    def test_by_800(self):
        self.assertTrue(divide_by_eight(800))
    def test_by_362880(self):
        self.assertTrue(divide_by_eight(362880))
