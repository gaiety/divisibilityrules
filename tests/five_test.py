from main import divide_by_five
import unittest

class TestDivideByFive(unittest.TestCase):
    def test_by_1(self):
        self.assertFalse(divide_by_five(1))
    def test_by_2(self):
        self.assertFalse(divide_by_five(2))
    def test_by_3(self):
        self.assertFalse(divide_by_five(3))
    def test_by_4(self):
        self.assertFalse(divide_by_five(4))
    def test_by_5(self):
        self.assertTrue(divide_by_five(5))
    def test_by_10(self):
        self.assertTrue(divide_by_five(10))
    def test_by_362880(self):
        self.assertTrue(divide_by_five(362880))
