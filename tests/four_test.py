from main import divide_by_four
import unittest

class TestDivideByFour(unittest.TestCase):
    def test_by_1(self):
        self.assertFalse(divide_by_four(1))
    def test_by_2(self):
        self.assertFalse(divide_by_four(2))
    def test_by_3(self):
        self.assertFalse(divide_by_four(3))
    def test_by_4(self):
        self.assertTrue(divide_by_four(4))
    def test_by_5(self):
        self.assertFalse(divide_by_four(5))
    def test_by_8(self):
        self.assertTrue(divide_by_four(8))
    def test_by_362880(self):
        self.assertTrue(divide_by_four(362880))
