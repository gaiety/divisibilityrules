from main import divide_by_three
import unittest

class TestDivideByThree(unittest.TestCase):
    def test_by_1(self):
        self.assertFalse(divide_by_three(1))
    def test_by_2(self):
        self.assertFalse(divide_by_three(2))
    def test_by_3(self):
        self.assertTrue(divide_by_three(3))
    def test_by_4(self):
        self.assertFalse(divide_by_three(4))
    def test_by_362880(self):
        self.assertTrue(divide_by_three(362880))
