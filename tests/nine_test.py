from main import divide_by_nine
import unittest

class TestDivideByNine(unittest.TestCase):
    def test_by_1(self):
        self.assertFalse(divide_by_nine(1))
    def test_by_3(self):
        self.assertFalse(divide_by_nine(3))
    def test_by_9(self):
        self.assertTrue(divide_by_nine(9))
    def test_by_10(self):
        self.assertFalse(divide_by_nine(10))
    def test_by_18(self):
        self.assertTrue(divide_by_nine(18))
    def test_by_362880(self):
        self.assertTrue(divide_by_nine(362880))
