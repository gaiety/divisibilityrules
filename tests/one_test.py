from main import divide_by_one
import unittest

class TestDivideByOne(unittest.TestCase):
    def test_by_1fractional(self):
        self.assertFalse(divide_by_one(1.5))
    def test_by_1(self):
        self.assertTrue(divide_by_one(1))
    def test_by_2(self):
        self.assertTrue(divide_by_one(2))
    def test_by_362880(self):
        self.assertTrue(divide_by_one(362880))
