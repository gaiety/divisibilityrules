from main import divide_by_six
import unittest

class TestDivideBySix(unittest.TestCase):
    def test_by_1(self):
        self.assertFalse(divide_by_six(1))
    def test_by_2(self):
        self.assertFalse(divide_by_six(2))
    def test_by_3(self):
        self.assertFalse(divide_by_six(3))
    def test_by_6(self):
        self.assertTrue(divide_by_six(6))
    def test_by_10(self):
        self.assertFalse(divide_by_six(10))
    def test_by_12(self):
        self.assertTrue(divide_by_six(12))
    def test_by_362880(self):
        self.assertTrue(divide_by_six(362880))
