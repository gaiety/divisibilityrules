from main import divide_by_two
import unittest

class TestDivideByTwo(unittest.TestCase):
    def test_by_1(self):
        self.assertFalse(divide_by_two(1))
    def test_by_2(self):
        self.assertTrue(divide_by_two(2))
    def test_by_3(self):
        self.assertFalse(divide_by_two(3))
    def test_by_4(self):
        self.assertTrue(divide_by_two(4))
    def test_by_362880(self):
        self.assertTrue(divide_by_two(362880))
